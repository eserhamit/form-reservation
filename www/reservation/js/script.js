
document.querySelector('form').addEventListener('submit', event => event.preventDefault());


url = 'https://gist.githubusercontent.com/revolunet/6173043/raw/222c4537affb1bdecbabcec51143742709aa0b6e/countries-FR.json';

fetch(url)
  .then(res => res.json())
  .then(data => {
    for (let paysTab of Object.keys(data)) {

      const selectPays = document.getElementById('pays');
      const option = document.createElement('option');
      option.setAttribute('value', paysTab);
      // ajouter un pays dans la balise  
      let pays = document.createTextNode(data[paysTab]);
      // Ajouter le pays dans la balise <option>
      option.appendChild(pays);
      // mettre la balise <option> dans la select qui a été selectioner grace à son id
      selectPays.appendChild(option);

    }
  })
  .catch(err => console.log("Error:", err));




/** Date start*/
let today = new Date();
let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

// Selectioner la date de depart
const inputDepart = document.getElementById('input-depart');
inputDepart.min = formatDate(date)


// Selectioner le div champsPersonnes
const champsPersonnes = document.getElementById('champsPersonnes');

//Selectioner le button qui a id "btnNombre"
const btn = document.getElementById('btnNombre');
btn.addEventListener('click', () => creerVoyageur());

creerVoyageur = () => {
  // Recuperer la valeur de nombre de personne
  
  let nombre = document.getElementById('nombrePersonne').value; 
  if(nombre==='')return
  btn.disabled = true;

  addVoyageur = () => {
    if (document.querySelectorAll('#champsPersonnes div')) {
      var p = document.querySelectorAll('#champsPersonnes div').length + 1;
      document.getElementById('nombrePersonne').value = p;

    } else {
      p = 1;
    }

    const voyageur = document.createElement("div");
    voyageur.setAttribute('class', 'mt-1 mb-1')

    champsPersonnes.insertBefore(voyageur, champsPersonnes.childNodes[p]);

    let textPersonne = document.createTextNode('Personne ' + p + ' ');
    let spanPersonne = document.createElement("span");
    spanPersonne.setAttribute('class', 'spanPersonne');
    voyageur.appendChild(spanPersonne);
    spanPersonne.appendChild(textPersonne);
    //voyageur.appendChild(textPersonne);

    //Creer voyageur
    let classV = new Voyageur();
    voyageur.appendChild(classV.ajouteChampsInput('text', 'name col-sm-12 col-md-3', 'Prènom'));
    voyageur.appendChild(classV.ajouteChampsInput('text', 'surname col-sm-12 col-md-3', 'Nom'));
    voyageur.appendChild(classV.ajouteChampsInput('date', 'birthday col-sm-12 col-md-3', ''));
    let btnDelete = creerBtn('btnDeleteId', 'btnDelete btn btn-danger text-light col-sm-12 col-md-12 col-lg-1', '<i class="fa fa-trash" aria-hidden="true"></i>');
    voyageur.appendChild(btnDelete);
    btnDelete.addEventListener('click', () => {
      //Suprimer le voyageur cliqué
      voyageur.remove();

      let spanPersonne = document.getElementsByClassName('spanPersonne');
      document.getElementById('nombrePersonne').value = spanPersonne.length;
      p = 1;
      // Après la supression de voyageur mettre à jour les nombre des voyageurs
      for (x = 0; x < spanPersonne.length; x++) {

        let textPersonne = document.createTextNode('Personne ' + p + ' ');
        spanPersonne[p - 1].innerHTML = '';
        spanPersonne[p - 1].appendChild(textPersonne);
        p++;
      }
    })
    voyageur.appendChild(document.createElement("span"));

  }// End addVoyageur function

  //Faire un boucle afin de créer des input pour chaque voyageur 
  for (x = 0; x < nombre; x++) {
    addVoyageur();
  }

  // S'il y a pas créer des button pour envoyer et ajouter
  if (!document.querySelector('#btnAjouter')) {

    let btnAjouter = champsPersonnes.appendChild(creerBtn('btnAjouter', 'btn btn-warning mr-2', 'Ajouter'));
    btnAjouter.addEventListener('click', addVoyageur);

    let btnEnvoyer = champsPersonnes.appendChild(creerBtn('btnEnvoyer', 'btn btn-primary', 'Envoyer'));
    btnEnvoyer.addEventListener('click', () => validation());
  }

  validation = () => {

    // Pour chaque <div> contenu dans le div avec l’id "champsPersonnes":
    for (const div of champsPersonnes.getElementsByTagName('div')) {
      // On récupère les données depuis les inputs associé
      const name = div.getElementsByClassName('name')[0].value;
      const surname = div.getElementsByClassName('surname')[0].value;
      const birthday = div.getElementsByClassName('birthday')[0].value;

      // On crée un objet voyageur
      const objetVoyageur = new Voyageur(name, surname, birthday);

      // Ajouter un message d'error à coté de voyageur 
      var span = div.getElementsByTagName('span')[1];
      if (objetVoyageur.validationObjet() != true) {
        span.innerText = objetVoyageur.validationObjet();
      } else {
        // s'il touc les champs sont rempli vider les spans
        span.innerText = '';
      }
    }
  }
}
/**Nombre de personne end */
