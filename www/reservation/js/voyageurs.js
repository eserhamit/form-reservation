class Voyageur {
    constructor(name, surname, birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    ajouteChampsInput(type, name, placeholder) {

        let input = document.createElement("input");
        input.setAttribute('type', type);
        input.setAttribute('class', name);
        input.setAttribute('placeholder', placeholder);
        return input;
    }

    validationObjet() {
        if (this.name == '' && this.surname == '' && this.birthday == '') {
            return 'veuillez remplir tous les champs';
        }
        if (this.name == '' && this.surname == '') {
            return 'veuillez remplir le prènom et le nom';
        }
        if (this.surname == '' && this.birthday == '') {
            return 'veuillez remplir le nom et la date';
        }
        if (this.name == '' && this.birthday == '') {
            return 'veuillez remplir le prènom et la date';
        }
        if (this.name == '') {
            return 'veuillez remplir le prènom';
        }
        if (this.surname == '') {
            return 'veuillez remplir le nom';
        }
        if (this.birthday == '') {
            return 'veuillez selectioner la date de naissance';
        } else {

            document.cookie = "name=" + this.name; //Crée ou met à jour un cookie
            document.cookie = "username=" + this.surname;
            document.cookie = "birthday=" + this.birthday;
            //console.log(document.cookie); //Affiche la liste des cookies

            return true;
        }
    }
}