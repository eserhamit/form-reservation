/**
 * Fonction pour creer des button 
 * @param {text} idName 
 * @param {text} className 
 * @param {text} text 
 */
function creerBtn (idName,className,text)
{
  const button = document.createElement("button"); 
  button.setAttribute('id',idName);
  button.setAttribute('class',className);
  button.innerHTML= text;

  return button;
}

  /**
   * ajoter '0' au débout de date
   * @param {date} date 
   */
  function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

