  // Set the date we're counting down to
  var countDownDate = new Date("Jun 11, 2020 07:37:25").getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="action"
  document.getElementById("action").innerHTML ="Il vous reste "+ days + " jours " + hours + "h "
  + minutes + "m " + seconds + "s pour bénéficier d’une remise de -10%";
 
  // S'il termine ajoute un text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("action").innerHTML = "Action est terminée";
  }
  // S'il reste moins d'un jour ajoute la classe rouge
  if (days < 1) {
    clearInterval(x);
    document.getElementById("action").className ='rouge';
  }
}, 1000);