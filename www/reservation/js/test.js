// Json list de pays ex2 

url = 'https://restcountries.eu/rest/v2/all';

fetch(url)
  .then(res => res.json())
  .then(data => {
    for (let paysTab of Object.keys(data)) {

      const selectPays = document.getElementById('pays');
      const option = document.createElement('option');
      option.setAttribute('value', data[paysTab].alpha2Code); 
      // ajouter un pays dans la balise  
      let pays = document.createTextNode(data[paysTab].name);
      // Ajouter le pays dans la balise <option>
      option.appendChild(pays);
      // mettre la balise <option> dans la select qui a été selectioner grace à son id
      selectPays.appendChild(option);

    }
  })
  .catch(err => console.log("Error:", err));